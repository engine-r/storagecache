describe('StorageCache', function () {

    before(function () {
        if (window.localStorage instanceof Storage) {
            localStorage.clear();
        }
    });

    after(function () {
        if (window.localStorage instanceof Storage) {
            localStorage.clear();
        }
    });

    describe('constructor( [namespace] )', function () {
        
        it('Если не передавать namespace конструктору, то this._namespace будет "cached"', function () {
            var a = new StorageCache();
            assert.equal('cached', a._namespace);
        });
        
        it('Переданный префикс записывается в this._namespace ', function () {
            var a = new StorageCache('test');
            assert.equal('test', a._namespace);
        });
        
    });
    
    describe('set(key, value [, expires])', function () {

        if (window.localStorage instanceof Storage) {
            localStorage.clear();
        }

        it('Переданные данные записываются в localStorage с переданным префиксом', function () {
            var a = new StorageCache('prefix');
            a.set('test', 1234);
            assert(a._storage['prefix-test'] !== undefined);
        });
        
        it('Если данные будут кэширован (localStorage поддерживается) возвращает true, иначе false', function () {
            var a = new StorageCache('test123');
            if (window.localStorage instanceof Storage) {
                assert(a.set('123', 456) === true);
            } else {
                assert(a.set('123', 456) === false);
            }
        })

    });
    
    describe('get(key)', function () {

        if (window.localStorage instanceof Storage) {
            localStorage.clear();
        }
        var a = new StorageCache('get(key)');

        it('Возвращает сохраненное значение', function () {
            var value1 = 1234567;
            var value2 = [1,2,3];
            var value3 = {a:1,b:2};

            a.set('test1', value1);
            a.set('test2', value2);
            a.set('test3', value3);
            assert.equal(a.get('test1'), value1);
            assert.typeOf(a.get('test2'), 'array');
            assert.equal(a.get('test2')[1], 2);
            assert.typeOf(a.get('test3'), 'object');
            assert.equal(a.get('test3').a, 1);
        });

        it('Если у записи истек срок жизни (expire) она удаляется и возвращается undefined', function (done) {
            a.set('expire', 'test', 500);
            setTimeout(function () {
                assert.equal(a.get('expire'), 'test');
                assert(a._storage['get(key)-expire'] != undefined);
            }, 100);
            setTimeout(function () {
                assert.equal(a.get('expire'), 'test');
                assert(a._storage['get(key)-expire'] != undefined);
            }, 300);
            setTimeout(function () {
                assert.equal(a.get('expire'), undefined);
                assert(a._storage['get(key)-expire'] === undefined);
                done();
            }, 600);
        })

    });

    describe('unset(key)', function () {

        if (window.localStorage instanceof Storage) {
            localStorage.clear();
        }
        var a = new StorageCache();

        it('Удаляет запись', function () {
            a.set('test', 1234);
            assert(a.get('test') === 1234);
            assert(a._storage['cached-test'] != undefined);
            a.unset('test');
            assert(a.get('test') === undefined);
            assert(a._storage['cached-test'] === undefined);
        });

    });

    describe('has(key)', function () {

        if (window.localStorage instanceof Storage) {
            localStorage.clear();
        }
        var a = new StorageCache();

        it('Проверяет, существует ли такая запись', function () {
            a.set('x', 'y');
            assert(a.has('x') === true);
            assert(a.has('z') === false);
        });

        it('Возвращает false и удаляет запись, если время жизни записи истекло', function (done) {
            a.set('a', 'b', 500);
            setTimeout(function () {
                assert(a.has('a') == true);
            }, 250);
            setTimeout(function () {
                assert(a.has('a') == false);
                done();
            }, 600);
        })

    });

    describe('clear()', function () {

        if (window.localStorage instanceof Storage) {
            localStorage.clear();
        }
        var a = new StorageCache('clear');

        it('Удаляет все записи из хранилища, относящиеся к пространству имен текущего кэша', function () {
            a._storage['some1'] = 1;
            a._storage['some2'] = 2;
            a._storage['some3'] = 3;
            a.set('deleteme1', 1);
            a.set('deleteme2', 2);
            a.set('deleteme3', 3);

            assert(a._storage['clear-deleteme1'] != undefined);
            assert(a._storage['clear-deleteme2'] != undefined);
            assert(a._storage['clear-deleteme3'] != undefined);

            a.clear();
            assert(a._storage['clear-deleteme1'] === undefined);
            assert(a._storage['clear-deleteme2'] === undefined);
            assert(a._storage['clear-deleteme3'] === undefined);
            assert(a._storage['some1'] == 1);
            assert(a._storage['some2'] == 2);
            assert(a._storage['some3'] == 3);
        })

    });
    
    describe('isCacheable()', function () {
        
        it('Возвращает true если данные будут кэшированны в localStorage, false в обратном случае', function () {
            var a = new StorageCache('clear');
            if (a._storage instanceof Storage) {
                assert(a.isCacheable() === true);
            } else {
                assert(a.isCacheable() === false);
            }
            
            a._storage = {};
            assert(a.isCacheable() === false);
        });
        
    })

});